/* Data proof */
INSERT INTO mwla.teachers(
	cod_teacher, first_name, middle_name, last_name, identity_card, age, genre, born_date, profession, speciality, start_time, end_time, workload, address, phone, created_at, updated_at)
	VALUES ('CCM-080183', PGP_SYM_ENCRYPT('Maria Ines','M&W)rk5-14**2020'), PGP_SYM_ENCRYPT('Conde','M&W)rk5-14**2020'), PGP_SYM_ENCRYPT('Callisaya','M&W)rk5-14**2020'), PGP_SYM_ENCRYPT('4939104LP','M&W)rk5-14**2020'),37,false,'1983-01-08','Lic en Informatica','Administradora GNU/Linux','2020-04-06 12:00:00','2020-04-06 14:00:00','72 Horas' , PGP_SYM_ENCRYPT('Av Alcides Arguedas Nro 1925','M7W0rk5-14**2020'), PGP_SYM_ENCRYPT('59177788112','M7W0rk5-14**2020'), '2020-04-01', '2020-04-01');

INSERT INTO mwla.teachers(
	cod_teacher, first_name, middle_name, last_name, identity_card, age, genre, born_date, profession, speciality, start_time, end_time, workload, address, phone, created_at, updated_at)
	VALUES ('ACA-090375', PGP_SYM_ENCRYPT('Andres Grover','M&W)rk5-14**2020'), PGP_SYM_ENCRYPT('Albino','M&W)rk5-14**2020'), PGP_SYM_ENCRYPT('Chambi','M&W)rk5-14**2020'), PGP_SYM_ENCRYPT('4273613LP','M&W)rk5-14**2020')::text,45,true,'1975-03-09','Lic en Informatica','fullstack web developer','2020-04-06 12:00:00','2020-04-06 14:00:00','72 Horas', PGP_SYM_ENCRYPT('Av Alcides Arguedas Nro 1925','M&W)rk5-14**2020')::text, PGP_SYM_ENCRYPT('59177794836','M&W)rk5-14**2020')::text,'2020-04-01', '2020-04-01');
	
INSERT INTO mwla.teachers(
	cod_teacher, first_name, middle_name, last_name, identity_card, age, genre, born_date, profession, speciality, start_time, end_time, workload, address, phone, created_at, updated_at)
	VALUES ('JOE-010370',PGP_SYM_ENCRYPT('Jeanneth rosmery','M&W)rk5-14**2020')::text, PGP_SYM_ENCRYPT('Ocampo','M&W)rk5-14**2020')::text, PGP_SYM_ENCRYPT('Eyzaguirre','M&W)rk5-14**2020')::text, PGP_SYM_ENCRYPT('5538744','M&W)rk5-14**2020')::text,42,TRUE,'1970-3-1','Ing. Informatico','Administrador de Redes', '2020-04-06 08:00','2020-04-06 09:30','96 Horas', PGP_SYM_ENCRYPT('Potosí','M&W)rk5-14**2020')::text, PGP_SYM_ENCRYPT('89900052','M&W)rk5-14**2020')::text,'2020-03-01','2020-03-01');

INSERT INTO mwla.students(
	cod_student, first_name, middle_name, last_name, identity_card, age, genre, born_date, address, phone, created_at, updated_at)
	VALUES ('DEA-090300', PGP_SYM_ENCRYPT('Daniel Armando','M&W)rk5-14**2020')::text, PGP_SYM_ENCRYPT('Espinoza','M&W)rk5-14**2020')::text, PGP_SYM_ENCRYPT('Alvarez','M&W)rk5-14**2020')::text, PGP_SYM_ENCRYPT('6678951OR','M&W)rk5-14**2020')::text, 20, false,'2010-11-21', PGP_SYM_ENCRYPT('Calle Estivaris Nro 2341','M&W)rk5-14**2020')::text, PGP_SYM_ENCRYPT('59177741410','M&W)rk5-14**2020')::text, '2020-02-03', '2020-02-03');
	
INSERT INTO mwla.students(
	cod_student, first_name, middle_name, last_name, identity_card, age, genre, born_date, address, phone, created_at, updated_at)
	VALUES ('MIA-010301', PGP_SYM_ENCRYPT('Miriam Estefania','M&W)rk5-14**2020')::text, PGP_SYM_ENCRYPT('Iturri','M&W)rk5-14**2020')::text, PGP_SYM_ENCRYPT('Aspi','M&W)rk5-14**2020')::text, PGP_SYM_ENCRYPT('6889745LP','M&W)rk5-14**2020')::text, 19, true,'2011-12-25', PGP_SYM_ENCRYPT('Edificio Torre Blanca Nro 2546','M7W0rk5-14**2020')::text, PGP_SYM_ENCRYPT('59161000000', 'M7W0rk5-14**2020')::text, '2020-02-03', '2020-02-03');

INSERT INTO mwla.matter(
	cod_matter, matter_name, description, created_at, updated_at)
	VALUES ('INF-111', 'INTRODUCCION A LA INFORMATICA', 'Introducion a la informatica','2020-04-01', '2020-04-01');

INSERT INTO mwla.matter(
	cod_matter, matter_name, description, created_at, updated_at)
	VALUES ('LAB-111', 'LABORATORIO DE INTRODUCCION A LA INFORMATICA', 'Laboratorio de Introducion a la informatica','2020-04-01', '2020-04-01');


INSERT INTO mwla.classroom(
	cod_classroom, location_classroom, created_at, updated_at)
	VALUES ('AUL-110', 'Edificio Central Piso 1', '2020-04-01', '2020-04-01');

INSERT INTO mwla.classroom(
	cod_classroom, location_classroom, created_at, updated_at)
	VALUES ('LAB-111', 'Edificio Central Planta baja', '2020-04-06', '2020-04-06');

INSERT INTO mwla.schedule(
	cod_schedule, cod_teacher, cod_student, cod_classroom, cod_matter, schedule, created_at, updated_at)
	VALUES ('horario-1', 'CCM080183', 'MIA-010301', 'AUL-110','INF-111', '2020-04-06 08:00', '2020-04-01', '2020-04-01');

INSERT INTO mwla.schedule(
	cod_schedule, cod_teacher, cod_student, cod_classroom, cod_matter, schedule, created_at, updated_at)
	VALUES ('horario-1', 'ACA-090375', 'DEA-090300', 'LAB-111','INF-111', '2020-04-06 10:00', '2020-04-01', '2020-04-01');

INSERT INTO mwla.assist_control(
    cod_schedule, date_time_assist, is_assist, is_permission, created_at, updated_at)
    VALUES('horario-1', '2020-04-06 10:00:29', true, false, '2020-04-06 12:00:00','2020-04-06 12:00:00');

INSERT INTO mwla.role(name, created_at, updated_at) VALUES('ROLE_STUDENT', '2020-04-06 12:00:00', '2020-04-06 12:00:00');
INSERT INTO mwla.role(name, created_at, updated_at) VALUES('ROLE_TEACHER', '2020-04-06 12:00:00', '2020-04-06 12:00:00');
INSERT INTO mwla.role(name, created_at, updated_at) VALUES('ROLE_ADMIN', '2020-04-06 12:00:00', '2020-04-06 12:00:00');

INSERT INTO mwla.user(name,username, email, password, created_at, updated_at) VALUES('Andres Grover Albino Chambi', PGP_SYM_ENCRYPT('grover.albino', 'M7W0rk5-14**2020')::text, PGP_SYM_ENCRYPT('grover.albino@gmail.com', 'M7W0rk5-14**2020')::text, PGP_SYM_ENCRYPT('MyWorks15','M7W0rk5-14**2020')::text,'2020-04-06 14:00:00', '2020-04-06 14:00:00');
INSERT INTO mwla.user(name,username, email, password, created_at, updated_at) VALUES('Maria Ines Conde Callisaya', PGP_SYM_ENCRYPT('ines.conde','M7W0rk5-14**2020')::text, PGP_SYM_ENCRYPT('edelynes@gmail.com','M7W0rk5-14**2020')::text, PGP_SYM_ENCRYPT('Edelynes','M7W0rk5-14**2020')::text,'2020-04-06 14:00:00', '2020-04-06 14:00:00');