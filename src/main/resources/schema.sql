/* Table Teacher */

CREATE TABLE mwla.teachers(
	id_teacher uuid PRIMARY KEY default uuid_generate_v4() NOT NULL,
	cod_teacher VARCHAR(11) NOT NULL,
	first_name text NOT NULL,
	middle_name text NULL,
	last_name text NOT NULL,
	identity_card text NOT NULL,
	age INT NOT NULL,
	genre BOOLEAN NOT NULL,
	born_date DATE NOT NULL,
	profession VARCHAR(50) NOT NULL,
	speciality VARCHAR(50) NOT NULL,
	start_time TIME NOT NULL,
	end_time TIME NOT NULL,
	workload VARCHAR(10) NOT NULL,
	address text NOT NULL,
	phone text NOT NULL,
	created_at TIMESTAMP NOT NULL,
	updated_at TIMESTAMP NOT NULL
);

/* Table Students */
CREATE TABLE mwla.students(
	id_student uuid PRIMARY KEY default uuid_generate_v4() NOT NULL,
	cod_student VARCHAR(11) NOT NULL,
	first_name text NOT NULL,
	middle_name text NULL,
	last_name text NOT NULL,
	identity_card text NOT NULL,
	age INT NOT NULL,
	genre BOOLEAN NOT NULL,
	born_date DATE NOT NULL,
	address text NOT NULL,
	phone text NOT NULL,
	created_at TIMESTAMP NOT NULL,
	updated_at TIMESTAMP NOT NULL
);

/* Table Matters */
CREATE TABLE mwla.matter(
	id_matter uuid PRIMARY KEY default uuid_generate_v4() NOT NULL,
	cod_matter VARCHAR(11) NOT NULL,
	matter_name VARCHAR(50) NOT NULL,
	description VARCHAR(100) NULL,
	created_at TIMESTAMP NOT NULL,
	updated_at TIMESTAMP NOT NULL
);

/* Table Classroom */
CREATE TABLE mwla.classroom(
	id_classroom uuid PRIMARY KEY default uuid_generate_v4() NOT NULL,
	cod_classroom VARCHAR(11) NOT NULL,
	location_classroom VARCHAR(80) NOT NULL,
	created_at TIMESTAMP NOT NULL,
	updated_at TIMESTAMP NOT NULL
);

/* Table Schedule */
CREATE TABLE mwla.schedule(
	id_schedule uuid PRIMARY KEY default uuid_generate_v4() NOT NULL,
	cod_schedule VARCHAR(11) NOT NULL,
	cod_teacher varchar(11) NOT NULL,
    cod_student varchar(11) NOT NULL,	
    cod_matter varchar(11) NOT NULL,
    cod_classroom varchar(11) NOT NULL,
	schedule varchar(30) NOT NULL,
	created_at TIMESTAMP NOT NULL,
	updated_at TIMESTAMP NOT NULL
);

/* Table AssistControl */
CREATE TABLE mwla.assist_control(
    id_assist_control uuid PRIMARY KEY default uuid_generate_v4() NOT NULL,
    cod_schedule VARCHAR(11) NOT NULL,
    date_time_assist TIMESTAMP NOT NULL,
    is_assist BOOLEAN NOT NULL,
    is_permission BOOLEAN NOT NULL,
	created_at TIMESTAMP NOT NULL,
	updated_at TIMESTAMP NOT NULL
);


/* Table Role*/
CREATE TABLE mwla.role(
    id_role uuid PRIMARY KEY default uuid_generate_v4() NOT NULL,
    name varchar(60) NOT NULL,
    created_at TIMESTAMP NOT NULL,
	updated_at TIMESTAMP NOT NULL
);

/* Table User*/
CREATE TABLE mwla.user(
    id_user uuid PRIMARY KEY default uuid_generate_v4() NOT NULL,
    name varchar(50) NOT NULL,
    username text NOT NULL,
    email text NOT NULL,
    password text,
    created_at TIMESTAMP NOT NULL,
	updated_at TIMESTAMP NOT NULL
);
