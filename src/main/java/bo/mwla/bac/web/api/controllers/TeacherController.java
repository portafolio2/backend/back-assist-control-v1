package bo.mwla.bac.web.api.controllers;

import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import bo.mwla.bac.web.api.entites.Teacher;
import bo.mwla.bac.web.api.services.TeacherService;

import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class TeacherController {

	@Autowired
	private TeacherService service;

	@GetMapping("/teachers")
	public List<Teacher> getAllTeachers() {
		return service.getAllTeacher();
	}

	@SneakyThrows
	@GetMapping("/teacher/id")
	public ResponseEntity<Teacher> getTeacherById(String codTeacher) {
		return service.getTeacherById(codTeacher);
	}
	
	@PostMapping("/teacher")
	public Teacher createTeacher(@RequestBody Teacher teacher) {
		return service.createTeacher(teacher);
	}
	
}
