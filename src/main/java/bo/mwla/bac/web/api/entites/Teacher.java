package bo.mwla.bac.web.api.entites;

import java.util.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.ColumnTransformer;

@Entity
@AllArgsConstructor
@NoArgsConstructor
public class Teacher {
	
	@Id
    private UUID idTeacher;

	@Column(name = "cod_teacher", length = 11)
	private String codTeacher;

	@ColumnTransformer(read = "pgp_sym_decrypt(first_name::bytea,'M7W0rk5-14**2020')", write = "pgp_sym_encrypt(?, 'M7W0rk5-14**2020')")
	private String firstName;

	@Column(name = "middle_name")
	@ColumnTransformer(read = "pgp_sym_decrypt(middle_name::bytea,‘M7W0rk5-14**2020’)", write = "pgp_sym_encrypt(?, ‘M7W0rk5-14**2020’)")
	private String middleName;

	@Column(name = "last_name")
	@ColumnTransformer(read = "pgp_sym_decrypt(last_name::bytea,‘M7W0rk5-14**2020’)", write = "pgp_sym_encrypt(?, ‘M7W0rk5-14**2020’)")
	private String lastName;

	@Column(name = "identity_card")
	@ColumnTransformer(read = "pgp_sym_decrypt(identity_card::bytea,‘M7W0rk5-14**2020’)", write = "pgp_sym_encrypt(?, ‘M7W0rk5-14**2020’)")
	private String identityCard;

	private Integer age;

	private Boolean genre;

	private Date born_date;

	@Column(name = "profession", length = 50)
	private String profession;

	@Column(name = "speciality", length = 50)
	private String speciality;

	private Date startTime;

	private Date endTime;

	@Column(name = "workload", length = 10)
	private String workload;

	@ColumnTransformer(read = "pgp_sym_decrypt(address::bytea,'M7W0rk5-14**2020')", write = "pgp_sym_encrypt(?,‘M7W0rk5-14**2020’)")
	private String address;

	@ColumnTransformer(read = "pgp_sym_decrypt(phone::bytea,‘M7W0rk5-14**2020’)", write = "pgp_sym_encrypt(?,‘M7W0rk5-14**2020’)")
	private String phone;

	private Date created_at;

	private Date updated_at;
}
