package bo.mwla.bac.web.api.services;

import bo.mwla.bac.web.api.exceptions.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import bo.mwla.bac.web.api.entites.Teacher;
import bo.mwla.bac.web.api.repositories.TeacherRepository;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@Service
public class TeacherService {

	@Autowired
	private TeacherRepository repository;

	/* CRUD Operations */
	/* List All Teachers*/
	public List<Teacher> getAllTeacher() {
		return repository.findAll();
	}

	/* List Teacher by Id */
	public ResponseEntity<Teacher> getTeacherById(@PathVariable(value = "cod_teacher") String codTeacher)
			throws ResourceNotFoundException {
		Teacher teacher = repository.findById(codTeacher)
				.orElseThrow(() -> new ResourceNotFoundException("Teacher not found for this id :: " + codTeacher));
		return ResponseEntity.ok().body(teacher);
	}

	/* Create a Teacher*/
	public Teacher createTeacher(@Valid @RequestBody Teacher teacher) {
		return repository.save(teacher);
	}

	/* Update a Teacher */
	public ResponseEntity<Teacher> updateTeacher(@PathVariable(value = "codTeacher") String codTeacher,
												   @Valid @RequestBody Teacher teacherDetails) throws ResourceNotFoundException {
		Teacher teacher = repository.findById(codTeacher)
				.orElseThrow(() -> new ResourceNotFoundException("Teacher not found for this id :: " + codTeacher));

		/* Updating Teacher fields */

		/*teacher.setEmailId(teacherDetails.getEmailId());
		teacher.setLastName(teacherDetails.getLastName());
		teacher.setFirstName(teacherDetails.getFirstName());*/
		final Teacher updatedTeacher = repository.save(teacher);
		return ResponseEntity.ok(updatedTeacher);
	}
}
