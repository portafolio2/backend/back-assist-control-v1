package bo.mwla.bac.web.api.entites;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Date;
import java.util.UUID;

@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Schedule {

    @Id
    private UUID idSchedule;

    private String codSchedule;

    private String codTeacher;

    private String codStudent;

    private String codMatter;

    private String codClassroom;

    private String schedule;

    private Date createdAt;

    private Date updatedAt;
}
