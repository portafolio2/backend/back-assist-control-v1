package bo.mwla.bac.web.api.entites;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.ColumnTransformer;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Date;
import java.util.UUID;

@Entity
@AllArgsConstructor
@NoArgsConstructor
public class Student {

    @Id
    private UUID idStudent;

    @Column(name = "cod_student", length = 11)
    private String codStudent;

    @ColumnTransformer(read = "pgp_sym_decrypt(first_name::bytea,'M7W0rk5-14**2020')", write = "pgp_sym_encrypt(?, 'M7W0rk5-14**2020')::text")
    private String firstName;

    @Column
    @ColumnTransformer(read = "pgp_sym_decrypt(middle_name::bytea,'M7W0rk5-14**2020')", write = "pgp_sym_encrypt(?, 'M7W0rk5-14**2020')::text")
    private String middleName;

    @Column
    @ColumnTransformer(read = "pgp_sym_decrypt(last_name::bytea,'M7W0rk5-14**2020')", write = "pgp_sym_encrypt(?, 'M7W0rk5-14**2020')::text")
    private String lastName;

    @Column
    @ColumnTransformer(read = "pgp_sym_decrypt(identity_card::bytea,'M7W0rk5-14**2020')", write = "pgp_sym_encrypt(?, 'M7W0rk5-14**2020')::text")
    private String identityCard;

    private int age;

    private Boolean genre;


    private Date bornDate;

    @Column
    @ColumnTransformer(read = "pgp_sym_decrypt(address::bytea,'M7W0rk5-14**2020')", write = "pgp_sym_encrypt(?, 'M7W0rk5-14**2020')::text")
    private String address;

    @Column
    @ColumnTransformer(read = "pgp_sym_decrypt(phone::bytea,'M7W0rk5-14**2020')", write = "pgp_sym_encrypt(?, 'M7W0rk5-14**2020')::text")
    private String phone;

    private Date createdAt;

    private Date updatedAt;

}
