package bo.mwla.bac.web.api.repositories;

import java.util.Optional;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import bo.mwla.bac.web.api.entites.Teacher;

@Repository
public interface TeacherRepository extends JpaRepository<Teacher, String>{
    Optional<Teacher> findById(String codTeacher);
}
