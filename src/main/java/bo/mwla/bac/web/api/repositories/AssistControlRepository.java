package bo.mwla.bac.web.api.repositories;

import bo.mwla.bac.web.api.entites.AssistControl;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface AssistControlRepository extends JpaRepository<AssistControl, UUID> {
}
