package bo.mwla.bac.web.api.entites;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Date;
import java.util.UUID;

@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Matter {

    @Id
    private UUID idMatter;

    private String codMatter;

    private String matterName;

    private String description;

    private Date createdAt;

    private Date updatedAt;

}
