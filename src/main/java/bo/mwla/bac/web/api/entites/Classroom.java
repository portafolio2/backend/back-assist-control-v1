package bo.mwla.bac.web.api.entites;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Date;
import java.util.UUID;

@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Classroom {

    @Id
    private UUID idClassroom;
    private String codClassroom;
    private String locationClassroom;
    private Date createdAt;
    private Date updatedAt;
}
