package bo.mwla.bac.web.api.entites;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Date;
import java.util.UUID;

@Entity
@NoArgsConstructor
@AllArgsConstructor
public class AssistControl {

    @Id
    private UUID idAssistControl;

    private String codSchedule;

    private Date dateTimeAssist;

    private Boolean isAssist;

    private Boolean isPermission;

    private Date createdAt;

    private Date updatedAt;
}
