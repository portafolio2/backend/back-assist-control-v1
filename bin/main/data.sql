/* Data proof */
INSERT INTO mwla.teacher(
	cod_teacher, first_name, middle_name, last_name, identity_card, age, genre, born_date, profession, speciality, start_time, end_time, workload, address, phone, email, created_at, updated_at)
	VALUES ('CCM080183','Maria Ines','Conde', 'Callisaya','4939104LP',37,false,'1983-01-08','Lic en Informatica','Administradora GNU/Linux','2020-04-06 12:00:00','2020-04-06 14:00:00','72 Horas' ,'Av Alcides Arguedas Nro 1925', '59177788112', 'edelynes@gmail.com', '2020-04-01', '2020-04-01');

INSERT INTO mwla.teacher(
	cod_teacher, first_name, middle_name, last_name, identity_card, age, genre, born_date, profession, speciality, start_time, end_time, workload, address, phone, email, created_at, updated_at)
	VALUES ('ACA-090375','Andres Grover','Albino','Chambi','4273613LP',45,true,'1975-03-09','Lic en Informatica','fullstack web developer','2020-04-06 12:00:00','2020-04-06 14:00:00','72 Horas','Av Alcides Arguedas Nro 1925', '59177794836', 'grover.albino@gmail.com', '2020-04-01', '2020-04-01');
	
INSERT INTO mwla.teacher(
	cod_teacher, first_name, middle_name, last_name, identity_card, age, genre, born_date, profession, speciality, address, phone, email, created_at, updated_at)
	VALUES ('JOE-010370','Jeanneth rosmery','Ocampo' ,'Eyzaguirre','5538744',42,TRUE,'1970-3-1','Ing. Informatico','Santa Cruz','Potosí','89900052','nheo_x_ing@hotmail.com','2020-03-01','2020-03-01')

INSERT INTO mwla.students(
	cod_student, first_name, middle_name, last_name, identity_card, age, genre, born_date, address, phone, email, created_at, updated_at)
	VALUES ('DEA-090300', 'Daniel Armando', 'Espinoza','Alvarez','6678951OR', 20, false,'2010-11-21', 'Calle Estivaris Nro 2341', '59177741410', 'dea2000@gmail.com', '2020-02-03', '2020-02-03');
	
INSERT INTO mwla.students(
	cod_student, first_name, middle_name, last_name, identity_card, age, genre, born_date, address, phone, email, created_at, updated_at)
	VALUES ('MIA-010301', 'Miriam Estefania','Iturri','Aspi', '6889745LP', 19, true,'2011-12-25','Edificio Torre Blanca Nro 2546', '59161000000', 'mia2001_@outlook.com', '2020-02-03', '2020-02-03');

INSERT INTO mwla.matter(
	cod_matter, matter_name, description, id_schedule, id_teacher, created_at, updated_at)
	VALUES ('INF-111', 'INTRODUCCION A LA INFORMATICA', 'Introducion a la informatica', 1, 1, '2020-04-01', '2020-04-01');

INSERT INTO mwla.schedule(
	id_schedule, id_matter, id_student, id_classroom, registration_date_time, created_at, updated_at)
	VALUES (1, 1, 1, 1, '2020-02-06', '2020-02-06', '2020-02-06');

INSERT INTO mwla.classroom(
	cod_classroom, classroom_name, classroom_location, created_at, updated_at)
	VALUES ('AUL-110', 'AULA 110', 'Edificio Central Piso 1', '2020-04-01', '2020-04-01');
	
INSERT INTO mwla.classroom(
	cod_classroom, classroom_name, classroom_location, created_at, updated_at)
	VALUES ('LAB-111', 'LABORATORIO DE DESARROLLO 111', 'Edificio Central Planta baja', '2020-04-06', '2020-04-06');
